# Alfred Mount Share

Workflow to list, connect and disconnect macOS built in VPN services [Alfred](https://www.alfredapp.com/). There are some others like it but this one is faster and better.

## Requirements

1. [Alfred Power Pack](https://www.alfredapp.com/powerpack/)
2. A configured VPN under macOS System Settings / Network

## Installation

1. Download [VPN Toggle.alfredworkflow](https://gitlab.com/mnewt/alfred-mount-share/raw/master/Mount%20Share.alfredworkflow)
2. Double click it to load it in Alfred
3. Alfred Settings -> Workflows -> Mount Share -> Click the ![[x]](x.png) in the upper right corner
4. Optionally, click the **+** and add your password **Workflow Environment Variables**. Your password will be automatically entered while connecting

| Name     | Value                          |
| -------- | ------------------------------ |
| password | `your-password`                |

## Usage

###

Type `vpn`, then space. You will see your VPNs and their status. To toggle the connect/disconnect status, hit `Enter`.

## Contributing

1. Fork it
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request

## History

2017-02-26: 1.0

## License

[MIT](LICENSE)
